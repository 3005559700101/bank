@extends('layouts.index')

@section('sidebar')
    @@parent
@stop

@section('content')
    <div class="toast-body">
        @isset($name)
            <p>Hello, {{$name}}</p>
            <br>
        @endisset
        <h2>Create an account</h2>
        <form action="/api/v1/account/create" method="POST">
            <div class="row">
                <div class="mb-6">
                    <label for="alias" class="form-label">Alias</label>
                    <input type="text" class="form-control" id="alias" name="alias">
                </div>
                <div class="mb-6">
                    <label for="monto" class="form-label">Amount</label>
                    <input type="number" class="form-control" id="monto" name="monto">
                </div>
            </div>
            <div class="row">
                <div class="mb-4" hidden="true">
                    <input type="text" class="form-control" id="cliente_id" name="cliente_id" value={{$id}}>
                </div>
                <div class="mb-8">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
        @isset($accounts)
            <div class="row">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Account</th>
                        <th scope="col">Name</th>
                        <th scope="col">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts as $account)
                        <tr>
                            <td>{{$account->id}}</td>
                            <td>{{$account->alias}}</td>
                            <td>{{$account->monto}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endisset
    </div>
@stop
