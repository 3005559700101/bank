@extends('layouts.index')

@section('sidebar')
    @@parent

@stop

@section('content')
    <div class="row">
        <h1 style="margin-top: 15px;">Well come!</h1>
        <h5>Please identify before use the bank system</h5>
    </div>
    <form action="/api/v1/login" method="POST">
        <div class="row">
            <div class="mb-6">
                <label for="document_type" class="form-label">Document type</label>
                <input type="text" class="form-control" id="document_type" name="document_type">
            </div>
            <div class="mb-6">
                <label for="identification" class="form-label">Identification</label>
                <input type="text" class="form-control" id="identification" name="identification">
            </div>
        </div>
        <div class="row">
            <div class="mb-4">
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <div class="mb-8">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@stop
