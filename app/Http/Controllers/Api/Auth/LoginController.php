<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class LoginController extends Controller
{
    public function login(Request $request){
        $credentials =  $request->validate([
           'document_type' => ['required'],
           'identification' => ['required'],
           'password' => ['required'],
        ]);

        $users = DB::select
        ('select * from login(?,?,?)',
            [
                $credentials['document_type'],
                $credentials['identification'],
                $credentials['password']
            ]);
        if (!$users){
            return view('partials.login');
        }
        $user = $users[0];
        if(!$user->cliente_razon_social){
            $user->cliente_razon_social = $user->cliente_nombres;
        }
        $request->session()->put('id', $user->cliente_id);
        return view('partials.welcome', ['id'=>$user->cliente_id, 'name' => $user->cliente_razon_social]);
    }
}
