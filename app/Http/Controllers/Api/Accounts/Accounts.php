<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Account;
use Illuminate\Support\Facades\DB;
class Accounts extends Controller
{
    public function create(Request $request){
        $params = $request->validate([
            'alias' => 'required',
            'monto'=> 'required',
            'cliente_id'=> 'required',
        ]);

        DB::insert(
            'insert into cuenta (alias, monto, cliente_id) values (?, ?, ?)',
            [
                $params['alias'],
                $params['monto'],
                $params['cliente_id']
            ]);
        $data = DB::select('select * from cuenta where cliente_id = ?', [$params['cliente_id']]);
        return view('partials/welcome', ['id'=>$params['cliente_id'], 'accounts'=>$data,]);
    }
}
