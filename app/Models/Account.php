<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'cuenta';

    protected $fillable = [
        'alias',
        'monto',
        'cliente_id'
    ];

    public function create($data){
        return Account::created($data);
    }
}
