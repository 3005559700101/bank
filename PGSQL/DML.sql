INSERT INTO departamento(
    departamento)
VALUES ('Guatemala');
select * from municipio;
INSERT INTO municipio(
    municipio, departamento_id)
VALUES ('Guatemala',1);
INSERT INTO municipio(
    municipio, departamento_id)
VALUES ('Mixco',1);
INSERT INTO municipio(
    municipio, departamento_id)
VALUES ('Villa Nueva',1);

INSERT INTO cliente(
    tipo_documento, identificacion, nombres, apellidos,  municipio_id, password)
VALUES ( 'DPI', '5263987852369', 'Juan Alfonso', 'Perez Castos', 1, 'mypass');

INSERT INTO cliente(
    tipo_documento, identificacion, nombres, apellidos,  municipio_id, password)
VALUES ( 'DPI', '2687635125698', 'Pedro Ricardo', 'Lopez Lopez', 2, 'myp@ss');

INSERT INTO cliente(
    tipo_documento, identificacion, razon_social, municipio_id, password)
VALUES ( 'Pasaporte', '9003356A', 'Los pollos hermanos', 3, 'tiktok');


CREATE OR REPLACE FUNCTION login(
    in_tipo_documento documento, in_identificacion varchar(12), in_password text) RETURNS
    TABLE ( cliente_id      INTEGER,
            cliente_nombres  VARCHAR,
		  	cliente_apellidos VARCHAR,
		  	cliente_razon_social VARCHAR) AS $$
BEGIN
RETURN QUERY
SELECT id, nombres, apellidos, razon_social FROM cliente
WHERE tipo_documento = in_tipo_documento
  AND identificacion = in_identificacion
  AND password = in_password;
END;
$$ LANGUAGE plpgsql;

drop function login;

select login('DPI', '2687635125698', 'myp@s')
