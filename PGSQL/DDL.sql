CREATE TYPE documento AS ENUM ('DPI', 'Pasaporte');

CREATE TABLE departamento (
    id SERIAL PRIMARY KEY,
    departamento VARCHAR(100) NOT NULL
);

CREATE TABLE municipio (
   id SERIAL PRIMARY KEY,
   municipio VARCHAR(100) NOT NULL,
   departamento_id INTEGER NOT NULL
);

alter table municipio
    add constraint fk_municipio_departamento
        foreign key (departamento_id)
            references departamento (id);

CREATE TABLE cliente (
     id SERIAL PRIMARY KEY,
     tipo_documento documento,
     identificacion VARCHAR(14),
     nombres VARCHAR(100),
     apellidos VARCHAR(100),
     razon_social VARCHAR(200),
     municipio_id INTEGER NOT NULL,
     password text
);
alter table cliente
    add constraint fk_cliente_municipio
        foreign key (municipio_id)
            references municipio (id);

CREATE TABLE cuenta (
    id SERIAL PRIMARY KEY,
    alias VARCHAR(100) NOT NULL,
    monto DECIMAL(12, 2) NOT NULL,
    cliente_id INTEGER NOT NULL
);
alter table cuenta
    add constraint fk_cuenta_cliente
        foreign key (cliente_id)
            references cliente (id);

CREATE TABLE movimiento (
    origen INTEGER NOT NULL,
    destino INTEGER NOT NULL,
    monto DECIMAL(10, 2) NOT NULL,
    descripcion TEXT,
    retiro  timestamp,
    deposito timestamp,
    dispositivo VARCHAR(100),
    id SERIAL PRIMARY KEY,
    autorizacion VARCHAR(100)
);
alter table movimiento
    add constraint fk_movimiento_cuenta_origen
        foreign key (origen)
            references cuenta (id),
add constraint fk_movimiento_cuenta_destino
foreign key (destino)
references cuenta (id);
