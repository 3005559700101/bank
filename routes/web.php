<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('partials.login');
});

Route::get('/home', function (){
   $id = session('id');
   if (!$id){
       return view('partials.login');
   }
   return view('partial.welcome', ['id'=>$id]);
});
